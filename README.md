## Assumptions
* You are using Chrome.
* You are familiar with copying and editing files.

## Create a copy of the Chrome extension
- Open `chrome://version` in Chrome and note your `Profile Path:` information.
- Open `about:extensions` in Chrome and turn on `Developer Mode` to note the
unique ID for the VTTA Assets extension.
- In the profile path folder, there is an `extensions` folder that contains a
folder with the unique extension ID. Copy that folder to a new location.
## Replace the monsters.js file
- In the copy of the extension, you will see a version folder similar to
`3.1.10_0`. In that folder is a `dndbeyond` folder.
- Download the [monsters.js](https://gitlab.com/csmcfarland/vtta-extension-workaround/-/blob/master/monsters.js)
file from this project and overwrite the one in your extension copy.
## Install the modified extension in Chrome
- Open `about:extension` in Chrome and make sure `Developer Mode` is enabled.
- Select `Load unpacked` and browse to the `3.1.10_0` folder in your copied
extension.

Helpful Links:
- https://gist.github.com/paulirish/78d6c1406c901be02c2d
